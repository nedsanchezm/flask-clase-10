from flask import *


app= Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/saludar')
def saludar():
    return ("Hola Mundo")

@app.route('/despedir')
def despedir():
    return ("Adios mundo cruel")

@app.route('/sumar', methods = ['POST'])
def sumar():
    misDatos=request.get_json()
    numero1= misDatos ['numero1']
    numero2= misDatos ['numero2']
    resultado= numero1+numero2
    return jsonify ({
        "resultado": resultado
    })
@app.route('/restar', methods = ['POST'])
def restar():
    misDatos=request.get_json()
    numero1= misDatos['numero1']
    numero2= misDatos ['numero2']
    resultado= numero1-numero2
    return jsonify ({
        "resultado": resultado
    })
@app.route('/dividir', methods = ['POST'])
def dividir():
    misDatos=request.get_json()
    numero1= misDatos ['numero1']
    numero2= misDatos ['numero2']
    resultado= numero1/numero2
    return jsonify ({
        "resultado": resultado
    })
@app.route('/multiplicar', methods = ['POST'])
def multiplicar():
    misDatos=request.get_json()
    numero1= misDatos ['numero1']
    numero2= misDatos ['numero2']
    resultado= numero1*numero2
    return jsonify ({
        "resultado": resultado
    })
@app.route('/modulo', methods = ['POST'])
def modulo():
    misDatos=request.get_json()
    numero1= misDatos ['numero1']
    numero2= misDatos ['numero2']
    resultado= numero1%numero2
    return jsonify ({
        "resultado": resultado
    })
app.run()
